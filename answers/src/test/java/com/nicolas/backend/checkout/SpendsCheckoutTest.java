package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Budget;
import com.nicolas.backend.domain.Cart;
import com.nicolas.backend.domain.Purchase;
import java.util.Collections;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class SpendsCheckoutTest extends CheckoutTestCase {

  @Test
  public void testSpendsCheckoutCalculateBudgetWithValuesUsingPricesMultipliedByQuantityPlusFees() {

    Purchase p = new Purchase();
    p.setArticles(buildArticles());
    p.setCarts(buildCarts());
    p.setDeliveryFees(Collections.singletonList(buildFee(10, 1000, 50)));
    SpendsCheckout checkout = new SpendsCheckout(p);

    Budget charge = checkout.charge();

    Assertions.assertThat(charge.getCarts())
        .hasSize(2)
        .extracting(Cart::getValue)
        .contains(170, 230);
  }
}