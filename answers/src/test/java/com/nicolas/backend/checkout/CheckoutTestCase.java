package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Article;
import com.nicolas.backend.domain.Cart;
import com.nicolas.backend.domain.DeliveryFee;
import com.nicolas.backend.domain.EligibleTransactionVolume;
import com.nicolas.backend.domain.Item;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class CheckoutTestCase {

  protected DeliveryFee buildFee(int minPrice, int maxPrice, int price) {
    DeliveryFee fee = new DeliveryFee();
    EligibleTransactionVolume volume = new EligibleTransactionVolume();
    volume.setMinPrice(minPrice);
    volume.setMaxPrice(maxPrice);
    fee.setEligibleTransactionVolume(volume);
    fee.setPrice(price);
    return fee;
  }

  protected List<Cart> buildCarts() {
    Cart cart1 = new Cart();
    cart1.setId(1);
    cart1.setItems(Arrays.asList(new Item(1, 2), new Item(2, 5)));

    Cart cart2 = new Cart();
    cart2.setId(2);
    cart2.setItems(Collections.singletonList(new Item(2, 9)));

    return Arrays.asList(cart1, cart2);
  }

  protected List<Article> buildArticles() {
    Article article1 = new Article(1, "a1", 10);
    Article article2 = new Article(2, "a2", 20);
    return Arrays.asList(article1, article2);
  }
}
