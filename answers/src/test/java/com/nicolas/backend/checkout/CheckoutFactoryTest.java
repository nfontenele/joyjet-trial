package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Purchase;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class CheckoutFactoryTest {

  @Test
  public void testFactoryCanGenerateSpendsDiscountsAndDefaultCheckout() {

    CheckoutFactory factory = new CheckoutFactory();

    Checkout checkoutDiscount = factory.createCheckout(Charger.DISCOUNT, new Purchase());
    Checkout checkoutSpends = factory.createCheckout(Charger.SPENDS, new Purchase());
    Checkout checkoutDefault = factory.createCheckout(Charger.DEFAULT, new Purchase());

    Assertions.assertThat(checkoutDefault).isInstanceOf(DefaultCheckout.class);
    Assertions.assertThat(checkoutSpends).isInstanceOf(SpendsCheckout.class);
    Assertions.assertThat(checkoutDiscount).isInstanceOf(DiscountCheckout.class);
  }

}