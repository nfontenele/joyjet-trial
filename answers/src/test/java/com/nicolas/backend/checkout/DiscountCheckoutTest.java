package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Budget;
import com.nicolas.backend.domain.Cart;
import com.nicolas.backend.domain.Discount;
import com.nicolas.backend.domain.Purchase;
import java.util.Arrays;
import java.util.Collections;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class DiscountCheckoutTest extends CheckoutTestCase {

  @Test
  public void testDiscountCheckoutCalculateBudgetWithValuesUsingPricesMultipliedByQuantityPlusFeesWithDiscount() {
    Purchase p = new Purchase();
    p.setArticles(buildArticles());
    p.setCarts(buildCarts());
    p.setDeliveryFees(Collections.singletonList(buildFee(10, 1000, 50)));
    p.setDiscounts(Collections.singletonList(new Discount(1, "amount", 30)));

    DiscountCheckout checkout = new DiscountCheckout(p);

    Budget charge = checkout.charge();

    Assertions.assertThat(charge.getCarts())
        .hasSize(2)
        .extracting(Cart::getValue)
        .contains(110, 230);
  }
}