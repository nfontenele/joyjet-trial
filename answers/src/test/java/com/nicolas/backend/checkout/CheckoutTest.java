package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Item;
import com.nicolas.backend.domain.Purchase;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class CheckoutTest {

  @Test
  public void testDefaultCheckoutInterfaceCanSumItemsFromPurchase() {

    Map<Integer, Integer> pricePerItem = new HashMap<>();
    pricePerItem.put(1, 10);
    pricePerItem.put(2, 20);

    Item item1 = new Item(1, 3);
    Item item2 = new Item(2, 5);

    Checkout checkout = new DefaultCheckout(new Purchase());

    int sumItems = checkout.sumItems(pricePerItem, Arrays.asList(item1, item2));

    Assertions.assertThat(sumItems).isEqualTo(130);
  }
}