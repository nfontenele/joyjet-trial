package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Article;
import com.nicolas.backend.domain.Budget;
import com.nicolas.backend.domain.Cart;
import com.nicolas.backend.domain.Item;
import com.nicolas.backend.domain.Purchase;
import java.util.Arrays;
import java.util.Collections;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class DefaultCheckoutTest extends CheckoutTestCase {

  @Test
  public void testDefaultCheckoutCalculateBudgetWithValuesUsingPricesMultipliedByQuantity() {

    Purchase p = new Purchase();
    p.setArticles(buildArticles());
    p.setCarts(buildCarts());

    DefaultCheckout checkout = new DefaultCheckout(p);
    Budget charge = checkout.charge();

    Assertions.assertThat(charge.getCarts())
        .hasSize(2)
        .extracting(Cart::getValue)
        .contains(120, 180);
  }
}