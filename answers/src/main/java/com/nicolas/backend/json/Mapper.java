package com.nicolas.backend.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

public final class Mapper {

  private ObjectMapper objectMapper = new ObjectMapper();

  public Optional<File> toFile(Object object, String path) {

    File output;
    try {
      path = System.getProperty("user.dir") + path;
      output = new File(path);
      objectMapper.writerWithDefaultPrettyPrinter().writeValue(output, object);
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException("Json processing error", e);
    } catch (IOException e) {
      throw new IllegalArgumentException("Problem when writing file to path: " + path, e);
    }
    return Optional.of(output);
  }

  public <T> T fromFile(File file, Class<T> type) {
    try {
      return objectMapper.readerFor(type).readValue(file);
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }
}
