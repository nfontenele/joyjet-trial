package com.nicolas.backend.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class Purchase {

  private List<Cart> carts = new ArrayList<>();
  private List<Article> articles = new ArrayList<>();

  @JsonProperty("delivery_fees")
  private List<DeliveryFee> deliveryFees = new ArrayList<>();
  private List<Discount> discounts = new ArrayList<>();

  public Purchase() {

  }
}
