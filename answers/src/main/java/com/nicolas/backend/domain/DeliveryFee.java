package com.nicolas.backend.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nicolas.backend.domain.EligibleTransactionVolume;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class DeliveryFee {

  @JsonProperty("eligible_transaction_volume")
  private EligibleTransactionVolume eligibleTransactionVolume;
  private int price;
}
