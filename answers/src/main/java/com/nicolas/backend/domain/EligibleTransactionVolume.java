package com.nicolas.backend.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class EligibleTransactionVolume {

  @JsonProperty("min_price")
  private int minPrice;
  @JsonProperty("max_price")
  private int maxPrice;
}
