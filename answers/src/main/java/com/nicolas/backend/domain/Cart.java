package com.nicolas.backend.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Cart {

  private int id;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<Item> items = new ArrayList<>();
  private int value = 0;
}
