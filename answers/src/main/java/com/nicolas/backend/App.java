package com.nicolas.backend;

import com.nicolas.backend.checkout.CheckoutFactory;
import com.nicolas.backend.service.CheckoutService;
import java.io.File;
import java.net.URL;
import java.util.Optional;

public class App {

  public static void main(String args[]) {

    URL url1 = App.class.getClassLoader().getResource("level1/data.json");
    URL url2 = App.class.getClassLoader().getResource("level2/data.json");
    URL url3 = App.class.getClassLoader().getResource("level3/data.json");

    String dataPathLevel1 = url1.getPath();
    String dataPathLevel2 = url2.getPath();
    String dataPathLevel3 = url3.getPath();

    CheckoutService checkoutService = new CheckoutService(new CheckoutFactory());

    System.out.println("Level 1 exercise");
    Optional<File> file1 = checkoutService.checkout(dataPathLevel1, "/output_level1.json");
    file1.ifPresent(file -> System.out.println("output file level1: " + file.getAbsolutePath()));

    System.out.println("Level 2 exercise");
    Optional<File> file2 = checkoutService.checkout(dataPathLevel2, "/output_level2.json");
    file2.ifPresent(file -> System.out.println("output file level3: " + file.getAbsolutePath()));

    System.out.println("Level 3 exercise");
    Optional<File> file3 = checkoutService.checkout(dataPathLevel3, "/output_level3.json");
    file3.ifPresent(file -> System.out.println("output file level3: " + file.getAbsolutePath()));
  }
}
