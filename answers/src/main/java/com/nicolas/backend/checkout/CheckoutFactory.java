package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Purchase;
import java.util.Objects;

import static com.nicolas.backend.checkout.Charger.DISCOUNT;
import static com.nicolas.backend.checkout.Charger.SPENDS;

public final class CheckoutFactory {
  public Checkout createCheckout(Charger charger, Purchase p) {
    Objects.requireNonNull(charger, "Charger should always be filled. 'Fail-First'");

    if (DISCOUNT.equals(charger)) {
      return new DiscountCheckout(p);
    } else if (SPENDS.equals(charger)) {
      return new SpendsCheckout(p);
    } else {
      return new DefaultCheckout(p);
    }
  }
}
