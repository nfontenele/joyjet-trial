package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Cart;
import com.nicolas.backend.domain.DeliveryFee;
import com.nicolas.backend.domain.Purchase;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class SpendsCheckout extends DefaultCheckout {

  public SpendsCheckout(Purchase purchase) {
    super(purchase);
  }

  @Override protected int calculateTotal(Map<Integer, Integer> pricePerItem, Cart cart) {
    int totalPrices = sumItems(pricePerItem, cart.getItems());
    return totalPrices + calculateFees(totalPrices, getPurchase().getDeliveryFees());
  }

  protected int calculateFees(int amount, List<DeliveryFee> deliveryFees) {
    int cost = 0;
    for (DeliveryFee fee : deliveryFees) {
      if (doesFeeApplyTo(amount).test(fee)) {
        cost = fee.getPrice();
        break;
      }
    }
    return cost;
  }

  private Predicate<DeliveryFee> doesFeeApplyTo(int amount) {
    return fee -> {
      int maxPrice = fee.getEligibleTransactionVolume().getMaxPrice() == 0 ? Integer.MAX_VALUE
          : fee.getEligibleTransactionVolume().getMaxPrice();
      int minPrice = fee.getEligibleTransactionVolume().getMinPrice();

      return (amount >= minPrice) && (amount <= maxPrice);
    };
  }
}
