package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Article;
import com.nicolas.backend.domain.Discount;
import com.nicolas.backend.domain.Purchase;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DiscountCheckout extends SpendsCheckout {

  public DiscountCheckout(Purchase purchase) {
    super(purchase);
  }

  @Override
  protected Map<Integer, Integer> getPricesPerItem() {
    Map<Integer, Discount> discountPerArticle = getPurchase().getDiscounts()
        .stream()
        .collect(Collectors.toMap(Discount::getArticleId, Function.identity()));

    return getPurchase().getArticles()
        .stream()
        .collect(Collectors.toMap(Article::getId, a -> {
          Discount discount = discountPerArticle.get(a.getId());
          return getPriceWithDiscount(a.getPrice(), discount);
        }));
  }

  private int getPriceWithDiscount(int finalPrice, Discount discount) {
    if (Objects.nonNull(discount)) {
      String type = discount.getType();
      if ("amount".equals(type)) {
        finalPrice = finalPrice - discount.getValue();
      } else if ("percentage".equals(type)) {
        int roundedUpDiscount = (int) Math.ceil(((double) finalPrice * discount.getValue() / 100));
        finalPrice = finalPrice - roundedUpDiscount;
      } else {
        throw new IllegalArgumentException("Invalid discount type");
      }
    }
    return finalPrice;
  }
}
