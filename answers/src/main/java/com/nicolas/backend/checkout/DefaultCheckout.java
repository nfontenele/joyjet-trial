package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Article;
import com.nicolas.backend.domain.Budget;
import com.nicolas.backend.domain.Cart;
import com.nicolas.backend.domain.Purchase;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DefaultCheckout implements Checkout {

  private final Purchase purchase;

  public DefaultCheckout(Purchase purchase) {
    this.purchase = purchase;
  }

  @Override
  public Budget charge() {

    Map<Integer, Integer> pricePerItem = getPricesPerItem();
    List<Cart> resultsCheckout = getCheckoutCarts(pricePerItem);

    Budget budget = new Budget();
    budget.setCarts(resultsCheckout);

    return budget;
  }

  protected List<Cart> getCheckoutCarts(Map<Integer, Integer> pricePerItem) {
    return purchase.getCarts()
        .stream()
        .map(cart -> {
          int total = calculateTotal(pricePerItem, cart);
          Cart c = new Cart();
          c.setId(cart.getId());
          c.setValue(total);
          return c;
        }).collect(Collectors.toList());
  }

  protected int calculateTotal(Map<Integer, Integer> pricePerItem, Cart cart) {
    return sumItems(pricePerItem, cart.getItems());
  }

  protected Map<Integer, Integer> getPricesPerItem() {
    return purchase.getArticles()
        .stream()
        .collect(Collectors.toMap(Article::getId, Article::getPrice));
  }

  public Purchase getPurchase() {
    return purchase;
  }
}
