package com.nicolas.backend.checkout;

import com.nicolas.backend.domain.Budget;
import com.nicolas.backend.domain.Item;
import java.util.List;
import java.util.Map;

public interface Checkout {

  Budget charge();

  default int sumItems(Map<Integer, Integer> pricePerItem, List<Item> items) {
    return items
        .stream()
        .mapToInt(item -> item.getQuantity() * pricePerItem.get(item.getArticleId()))
        .sum();
  }
}
