package com.nicolas.backend.service;

import com.nicolas.backend.checkout.Charger;
import com.nicolas.backend.checkout.Checkout;
import com.nicolas.backend.checkout.CheckoutFactory;
import com.nicolas.backend.domain.Budget;
import com.nicolas.backend.domain.Purchase;
import com.nicolas.backend.json.Mapper;
import java.io.File;
import java.util.Optional;

import static org.apache.commons.collections.CollectionUtils.*;

/**
 * Manual Test written in 'App' class. Normally it would be removed and do it for this class
 */
public class CheckoutService {

  private final Mapper mapper = new Mapper();

  private final CheckoutFactory checkoutFactory;

  public CheckoutService(CheckoutFactory checkoutFactory) {
    this.checkoutFactory = checkoutFactory;
  }

  public Optional<File> checkout(String dataPath, String outputPath) {

    Purchase p = mapper.fromFile(new File(dataPath), Purchase.class);
    Checkout checkout = checkoutFactory.createCheckout(getCharger(p), p);

    Budget budget = checkout.charge();
    return mapper.toFile(budget, outputPath);
  }

  private Charger getCharger(Purchase p) {
    Charger charger = Charger.DEFAULT;
    if (isNotEmpty(p.getDeliveryFees()) && isNotEmpty(p.getDiscounts())) {
      charger = Charger.DISCOUNT;
    } else if (isNotEmpty(p.getDeliveryFees())) {
      charger = Charger.SPENDS;
    }
    return charger;
  }
}
