##Design Considerations:
 - Simplified classes to be consistent with model. However in production project 
 it should evaluate to use custom serializer. Ex: An 'Item' might have an Article instead of 'articleId'. Since
 this approach makes more sense on DB level and not Object level.
 
## Unit testing
    - Not testing json (de)serializing since it uses the api pretty much directly. Much effort to smaller gain
    for exercise.
    - Haven't tested the service class. The test would be pretty much what 'App' class does. So I think
    its not necessary
    - Mostly logic are on checkout::charge feature. So most test are there which I think its the ones 
    actually useful for exercise.
    
 
##Running and checking results
  - for build simple 'mvn clean install'
  - you can check 'checkout' unit tests
  - you can just run App class and check json results on project directory
  
  
Let me know any questions or doubts. I can of course improve more but I think this version is enough clean.